from queue import Empty
from fastapi.testclient import TestClient
from main import app
from postgres.db import CategoryQueries


client = TestClient(app)


class EmptyCategoryQueries:
    def get_category(self, id):
        return None


class NormalCategoryQueries:
    def get_category(self, id):
        return [id, "OUR CATEGORY", True]


class NormalCategoriesQueries:
    def get_all_categories(self, page: int):
        return 1, [[1, "OUR CATEGORY", True, 0], [2, "ANOTHER ONE", False, 2]]


def test_get_category_returns_404():
    #Arrange
    #Use the fake db
    app.dependency_overrides[CategoryQueries] = EmptyCategoryQueries

    #Act
    #Make the request
    response = client.get("/api/postgres/categories/1")

    #Assert
    #Assert that we got a 404
    assert response.status_code == 404

    #Clean up
    #Clear out the dependencies
    app.dependency_overrides = {}


def test_get_category_returns_200():
    #Arrange
    app.dependency_overrides[CategoryQueries] = NormalCategoryQueries

    #Act
    response = client.get("/api/postgres/categories/1")
    d = response.json()

    #Assert
    assert response.status_code == 200
    assert d["id"] == 1
    assert d["title"] == "OUR CATEGORY"
    assert d["canon"] == True

    #Clean up
    app.dependency_overrides = {}


def test_get_categories_returns_list():
    #Arrange
    app.dependency_overrides[CategoryQueries] = NormalCategoriesQueries

    #Act
    response = client.get("/api/postgres/categories/")
    d = response.json()

    #Assert
    assert response.status_code == 200
    assert d["page_count"] == 1
    assert len(d["categories"]) == 2
    assert d["categories"][0]["id"] == 1

    #Clean up
    app.dependency_overrides = {}